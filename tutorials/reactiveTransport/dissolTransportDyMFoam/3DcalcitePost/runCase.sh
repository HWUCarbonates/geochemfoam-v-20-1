#!/bin/bash




# ###### DO NOT MAKE CHANGES FROM HERE ###################################



set -e


python << END
import os


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


a=0;
s=str(0)

for n in range(0,10801,1800):
  os.system('cp system/controlDictRun system/controlDict')
  os.system('sed -i "s/var/'+str(n)+'/g" system/controlDict') 
  while a<n:
    os.system('decomposePar')
    os.system('mpiexec -np 12 dissolTransportDyMFoam -parallel')
    os.system('reconstructPar' )
    os.system('rm -rf processor*')
    for directories in os.listdir(os.getcwd()): 
      if (is_number(directories)):
        if (float(directories)>a):
          a=float(directories)
          s=directories
    os.system( './remesh.sh') 
    os.system('mapFields ../3DcalcitePost -case ../temp -sourceTime latestTime')
    os.system('mv ../temp/0/pointMotionU* ../temp/0/pointMotionU')
    print('rm -rf '+s)
    os.system('rm -rf '+s)
    os.system('mv ../temp/0 ./'+s)
    os.system('rm -rf ../temp')
    os.system('cp constant/triSurface/calcitePost_org.stl constant/triSurface/calcitePost.stl')
END

processPoroPerm


