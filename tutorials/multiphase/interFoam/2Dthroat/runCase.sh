#!/bin/bash

set -e

blockMesh
cp 0/alpha.water.org 0/alpha.water
setFields
decomposePar
mpiexec -np 4 interFoam  -parallel
reconstructPar
rm -rf proc*
